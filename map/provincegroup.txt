
golden_highway_bulwar_proper = {
	565 567 572 561 584 596 598 600 601 602 608 613 628
}
golden_highway_far_bulwar = {
	625 626 627 631 634 639 640 641 642 643 647 558 585
}
golden_highway_far_salahad = {
	2900 2901 2902 2905 2908 2910 2913
}

irrigation_dwarovar = {
	4125 4129 4123 4142 4131 4143 4158 4151 4152 4166 4164 4145
}

west_dolindha = {
	1823 2842 1824 1818 1820 1819 2809 1187 1188 1186 2813 1167 2808 2812 1168 1169 1170 1182 1755
}

east_dolindha = {
	1166 2811 2837 1165 2862 2839 1172 2814 1171 1180 2742 2840 2488 2838
}

deepwoods_cluster_1 = {
	3005 3004 3006 3007 3008
}

deepwoods_cluster_2 = {
	3018 3019 3020 3021 3022
}

deepwoods_cluster_3 = {
	3013 3014 3015 3016 3017
}

deepwoods_cluster_4 = {
	3009 3010 3011 3012 3067
}

deepwoods_cluster_5 = {
	3027 3028 3029 3030 3031
}

deepwoods_cluster_6 = {
	3023 3024 3025 3026
}

deepwoods_cluster_7 = {
	3032 3033 3034 3035 3036 3037 3038
}

deepwoods_cluster_8 = {
	3039 3040 3041 3042
}

deepwoods_cluster_9 = {
	3043 3044 3045 3046 3047
}

deepwoods_outward_gladeway = {
	3071 3048 3054 3068 3065 3064
}