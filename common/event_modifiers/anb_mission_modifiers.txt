
generic_into_anbennar = {
	ae_impact = -0.2
	manpower_recovery_speed = 0.1
}

generic_into_lorent = {
	ae_impact = -0.2
	manpower_recovery_speed = 0.1
}

lorent_lorentish_naval_drills = {
	galley_power = 0.25
	naval_maintenance_modifier = -0.1
}

lorent_reformed_knights_of_the_rose = {
	cavalry_power = 0.15
}

lorent_emperor_disregards_lorentish_advances = {
	ae_impact = -0.2
}

lorent_mages_of_the_ruby_order = {

}

lorent_ruby_crown_awakened = {
	yearly_absolutism = 0.1
	legitimacy = 1
	monarch_admin_power = 2
	monarch_diplomatic_power = 2
	monarch_military_power = 2
}

gawed_gawedi_settlers = {
	colonists = 1
	colonist_placement_chance = 0.05
}

overclan_rebuild_sur-els_temple = {
	religious_unity = 0.20
	tolerance_heathen = 1
	religion = yes
}

burning_of_the_redglades = {
	local_development_cost = 1
	local_production_efficiency = -0.5
}

bane_of_elfkind = {
	unjustified_demands = -0.2
	land_morale = 0.05
}

merchant_navy = {
	global_ship_trade_power = 0.4
}

deranne_springboard = {
	range = 0.5
	colonist_placement_chance = 0.2
}

deranne_seaflower_levy = {
	infantry_power = 0.1
	infantry_cost = -0.1
	yearly_army_professionalism = 0.01
}

deranne_seaflower_knights = {
	cavalry_power = 0.1
	heavy_ship_power = 0.1
	yearly_army_professionalism = 0.01
}

deranne_winebay_bastion = {
	fort_level = 1
	local_defensiveness = 0.25
	local_manpower_modifier = 0.1
	local_friendly_movement_speed = 0.1
	local_hostile_movement_speed = 0.1
	garrison_growth = 0.05
	local_development_cost = -0.1
}

deranne_aelvar_wood = {
	global_ship_cost = -0.25
	global_ship_recruit_speed = -0.25
}

deranne_reaver_wine = {
	local_production_efficiency = 0.1
	local_tax_modifier = 0.1
	trade_goods_size = 0.5
	trade_goods_size_modifier = 0.25
	local_development_cost = -0.1
}

deranne_new_adea_naval_school = {
	dip_tech_cost_modifier = -0.1
	light_ship_power = 0.1
	naval_forcelimit_modifier = 0.1
	naval_maintenance_modifier = -0.1
	admiral_cost = -0.1
	admiral_skill_gain_modifier = 0.1
}

deranne_southern_hegemony = {
	prestige = 0.5
	yearly_absolutism = 0.1
	max_states = 3
	legitimacy = 1
	republican_tradition = 0.5
	devotion = 1
}

deranne_soruin_colonizer = {
	global_colonial_growth = 20
	range = 0.5
	global_tariffs = 0.25
}

deranne_reaver_conquest = {
	years_of_nationalism = -5
	promote_culture_cost = -0.25
	culture_conversion_cost = -0.25
}

deranne_army_of_deranne = {
	yearly_army_professionalism = 0.05
	drill_gain_modifier = 0.1
	drill_decay_modifier = -0.1
	discipline = 0.05
	land_maintenance_modifier = -0.05
	leader_cost = -0.1
}

#Eborthil Mission modifiers

# Stonegaze toll
stonegaze_toll_modifier = {
	local_institution_spread = 0.10
	province_trade_power_value = 10
	naval_forcelimit = 2
	local_development_cost = -0.05
	local_sailors_modifier = 0.50
	picture = "province_trade_power_value"
}

#Ourdian Mission modifiers. 

ourdi_protected_trade = {
	global_own_trade_power = 0.15
	trade_efficiency = 0.1
	ship_power_propagation = 0.1
}

ourdi_settlers_stream = {
	culture_conversion_cost = -0.1
	global_missionary_strength = 0.01
}

ourdi_crusading_zeal = {
	tolerance_own = 1
	tolerance_heathen = -1
	global_missionary_strength = 0.01
}

ourdi_favoured_by_corin = {
	land_morale = 0.1
	naval_morale = 0.1
	discipline = 0.075
	global_unrest = -2
}

ourdi_empire_of_men = {
	core_creation = -0.20
	global_unrest = -2
	tolerance_heathen = -4
	global_missionary_strength = 0.04
}

ourdi_colonial_fervor = {
	colonist_placement_chance = 0.05
	global_colonial_growth = 20
}
	
ourdi_awakening_the_economy = {
	development_cost = -0.1
	production_efficiency = 0.1
}

ourdi_bustling_trade = {
	global_trade_power = 0.15
	ship_power_propagation = 0.1
	naval_maintenance_modifier = -0.1
}

ourdi_dostanorian_unity = {
	global_unrest = -2
	prestige = 2
	legitimacy = 2
	republican_tradition = 0.25
	devotion = 0.25
}

ourdi_castanorian_lessons = {
	defensiveness = 0.1
	build_cost = -0.1
	build_time = -0.3
	fort_maintenance_modifier = -0.05
}
nathalaire_seizing_of_assets = {
global_tax_modifier = 0.15
}

nathalaire_resettlement_of_goblins = {
colonists = 1
colonist_placement_chance = 0.05
global_colonial_growth = 25
}
nathalaire_gems_of_the_gem_isle = {
trade_goods_size = 0.5
}
               # Adventurer Mission Tree modifier
adv_forcelimit_modifier = {
	monthly_militarized_society = 0.05
}

adv_colonist_modifier = {
	global_colonial_growth = 25
	colonist_placement_chance = 0.05
}

adv_war_ork_modifier = {
	land_morale = 0.05
	infantry_power = 0.05
}

adv_colon_modifier = {
	colonists = 1
}

adv_grain_production = {
	trade_goods_size = 2
}

adv_big_forge = {
	trade_goods_size = 1.5
}

adv_dev = {
	development_cost = -0.2
}

adv_guild_integrated = {
	tax_income = 12
	administrative_efficiency = 0.05
}

adv_gems = {
	trade_goods_size = 1
}

adv_bank = {
	trade_goods_size = 1
}

adv_build_modifier = {
	build_cost = -0.2
	build_time = -0.25
}

adv_home_trade = {
	trade_efficiency = 0.25
}

adv_trade_powerhouse = {
	merchants = 1
}

adv_fame = {
	diplomatic_reputation = 1
}

adv_standing_army = {
	infantry_power = 0.05
}

adv_administration = {
	trade_goods_size = 1.5
}

     #   Magisterium
	 
expedition_ready = {
	global_tax_modifier = -0.1
	production_efficiency = -0.1
	trade_efficiency = -0.1
	administrative_efficiency = -0.05
}

skyfall_damestear = {
	trade_goods_size = 3
}

magic_upsurge = {
	all_power_cost = -0.05
}

mag_tower_base = {
	local_development_cost = -0.05
}

mag_tower_one = {
	local_state_maintenance_modifier = 0.05
	local_development_cost = -0.075
}

mag_tower_two = {
	local_state_maintenance_modifier = 0.10
	local_development_cost = -0.1
}

mag_tower_three = {
	local_state_maintenance_modifier = 0.15
	local_development_cost = -0.125
	tax_income = 6
}

mag_tower_four = {
	local_state_maintenance_modifier = 0.2
	local_development_cost = -0.15
	tax_income = 9
	global_tax_modifier = 0.025
}

mag_tower_five = {
	local_state_maintenance_modifier = 0.25
	local_development_cost = -0.175
	tax_income = 12
	global_tax_modifier = 0.05
	production_efficiency = 0.025
	all_power_cost = -0.025
}

mag_tower_six = {
	local_state_maintenance_modifier = 0.3
	local_development_cost = -0.2
	tax_income = 12
	global_tax_modifier = 0.075
	production_efficiency = 0.05
	all_power_cost = -0.05
}

mag_tower_seven = {
	local_state_maintenance_modifier = 0.35
	local_development_cost = -0.25
	tax_income = 12
	global_tax_modifier = 0.1
	production_efficiency = 0.075
	all_power_cost = -0.075
}

mag_tower_eight = {
	local_state_maintenance_modifier = 0.5
	local_development_cost = -0.3
	tax_income = 12
	global_tax_modifier = 0.1
	production_efficiency = 0.1
	all_power_cost = -0.1
}

mag_breakthrought = {
	local_development_cost = -0.5
}

mage_stash = {
	trade_goods_size = 1.5
}

mag_big_laboratory = {
	trade_goods_size = 1.5
}

magic_reinforced_wall = {
	local_defensiveness = 0.2
}
birsartanses_subjugate_monsters = {
global_unrest = -1
missionary_maintenance_cost = -0.2
}
birsartanses_vassalize = {
global_autonomy = -0.05
harsh_treatment_cost = -0.2
}
birsartanses_prestigious = {
manpower_recovery_speed = 0.1
}
birsartanses_expand = {
core_creation = -0.1
}

#Nimscodd mission modifiers

nimscodd_religious_tolerance = {
	tolerance_heathen = 1
	tolerance_heretic = 1
}

nimscodd_peace_religion = {
	tolerance_own = 1
	tolerance_heathen = 1
	tolerance_heretic = 1
}

nimscodd_the_gnomish_trade_empire = {
	global_trade_power = 0.1
}

nimscodd_the_heirarch_designed = {
	ship_durability = 0.05
	heavy_ship_power = 0.1
}

nimscodd_new_prototypes_developed = {
	fire_damage = 0.05
}

nimscodd_joint_dwarven_gnomish_engineering_corps = {
	defensiveness = 0.05
	artillery_power = 0.1
}

gnomish_workshops = {
	trade_goods_size = 1
}

nimscodd_resettling_the_homeland = {
	culture_conversion_cost = -0.5
}

   # Venail
venail_cestircel = {
	province_trade_power_value = 10
}

damescrown_payment = {
	global_tax_income = -30
}

damescrown_funding = {
	colonists = 1
	colonist_placement_chance = 0.1
	global_colonial_growth = 60
	global_tax_income = 40
	manpower_recovery_speed = -0.4
}

damescrown_funding_ai = {
	colonists = 1
	colonist_placement_chance = 0.1
	global_colonial_growth = 60
	global_tax_income = 120
	manpower_recovery_speed = -0.4
}

alariandel_impulse = {
	range = 1.5
	colonists = -2
}

capital_influx = {
	local_development_cost = -0.5
}

land_drained = {
	local_development_cost = 0.5
	trade_goods_size = -1
}

venail_ressource_influx = {
	global_colonial_growth = 20
}

venail_immigration_first_step = {
	trade_efficiency = -0.1
	global_tax_modifier = -0.1
	production_efficiency = -0.1
	administrative_efficiency = -0.05
}

venail_immigration_second_step = {
	trade_efficiency = -0.2
	global_tax_modifier = -0.2
	production_efficiency = -0.2
	administrative_efficiency = -0.1
}

venail_immigration_third_step = {
	trade_efficiency = -0.35
	global_tax_modifier = -0.35
	production_efficiency = -0.35
	administrative_efficiency = -0.2
	development_cost = 0.5
}

venail_immigration_complete = {
	development_cost = -0.5
	global_colonial_growth = 80
}

venail_repay_loan = {
	global_tax_income = -40
}

venail_damescrown_receive_loan = {
	global_tax_income = 40
}
venail_autonomy = {
	global_autonomy = -0.05
}

venail_economic_boom = {
	global_trade_goods_size_modifier = 0.1
}

venail_trading_boom = {
	trade_efficiency = 0.15
}

venail_legion = {
	land_morale = 0.1
	infantry_power = 0.1
}

venail_new_fleet = {
	naval_maintenance_modifier = -0.2
}

mischievious_elf = {
	diplomatic_reputation = -1.5
	improve_relation_modifier = -0.2
}

venail_ai_boost = {
	global_tax_income = 72
	colonists = 3
	global_colonial_growth = 100
}

## Aelnar

aelnar_castle_fondation = {
	local_development_cost = 0.1
}


aelnar_commoner_quarter = {
	local_development_cost = 0.1
}

aelnar_arca_ore = {
	local_defensiveness = 0.1
	local_development_cost = -0.1
	max_states = 3
	prestige = 0.5
}

aelnar_drill_academy = {
	infantry_power = 0.1
	land_maintenance_modifier = -0.1
}

aelnar_food_influx = {
	production_efficiency = 0.1
	global_colonial_growth = 15
}

aelnar_purity_effort = {
	global_missionary_strength = 0.02
	culture_conversion_cost = -0.25
}

aelnar_ynn_purity_done = {
	global_trade_goods_size_modifier = 0.1
}

aelnar_trade_efficiency = {
	trade_efficiency = 0.15
}

aelnar_northern_wall = {
	local_defensiveness = 0.25
}

aelnar_fleet = {
	ship_durability = 0.05
	naval_maintenance_modifier = -0.1
}

aelnar_firstborn = {
	global_colonial_growth = 200
}

aelnar_same_people = {
	same_culture_advisor_cost = -0.33
}

aelnar_citizenship = {
	num_accepted_cultures = -2
}

aelnar_genocide = {
	local_unrest = -1
	trade_goods_size_modifier = -0.5
}

aelnar_racial_purity = {
	global_unrest = -1
	max_states = 20
}

aelnar_star_ascendancy = {
	global_missionary_strength = 0.01
	global_trade_goods_size_modifier = 0.05
	trade_efficiency = 0.05
	production_efficiency = 0.05
	global_tax_modifier = 0.05
	administrative_efficiency = 0.1
	land_forcelimit_modifier = 0.15
	naval_forcelimit_modifier = 0.15
}

aelnar_spices_flow = {
	production_efficiency = 0.25
}

aelnar_central_hegemony = {
	naval_maintenance_modifier = -0.33
}

aelnar_kheionia_trade = {
	trade_efficiency = 0.1
}

aelnar_south_legion = {
	land_forcelimit_modifier = 0.15
}

aelnar_making_money = {
	inflation_reduction = -1
	inflation_action_cost = 0.5
	global_tax_income = 24
}

aelnar_holy_knight_maintenance = {
	global_manpower = -0.1
	reinforce_speed = -0.1
	army_tradition = 0.5
}

aelnar_calasandur_coup = {
	global_unrest = 1
}

aelnar_starseeker_legacy = {
	land_morale = 0.05
}

aelnar_seawatcher_legacy = {
	naval_morale = 0.05
	trade_efficiency = 0.05
	ship_power_propagation = 0.1
}

aelnar_civil_war_aftermath = {
	global_colonial_growth = -40
	administrative_efficiency = -0.2
	global_unrest = -2
	manpower_recovery_speed = -0.25
	min_autonomy = 10
}

aelnar_civil_war_bloody_aftermath = {
	global_colonial_growth = -80
	administrative_efficiency = -0.25
	manpower_recovery_speed = -0.33
	colonists = -1
	all_power_cost = 0.05
	min_autonomy = 25
}

aelnar_exclusive_trade = {
	trade_goods_size = 1
}

aelnar_manifest_destiny = {
	colonists = 1
}

aelnar_lithiel_in_prison = {
	stability_cost_modifier = -0.05
}

aelnar_lithiel_rule = {
	land_morale = 0.025
	stability_cost_modifier = -0.10
}

aelnar_the_crystal_queen = {
	land_morale = 0.05
	stability_cost_modifier = 0.15
}

aelnar_icy_ambitions = {
	harsh_treatment_cost = -0.25
	years_of_nationalism = -5
	global_unrest = 2
	min_autonomy_in_territories = 0.1
	max_states = 5
	ae_impact = 0.1
	improve_relation_modifier = -0.25
}

aelnar_the_crystal_tiara = {
	legitimacy = 1
	monthly_splendor = 1
}

aelnar_nur_meweri = {
	prestige = 0.5
	allowed_num_of_buildings = 1
	trade_value_modifier = 1
	trade_goods_size = 1
	local_defensiveness = 1
	max_states = 3
	picture = "nur_meweri"
}

aelnar_draft_integration_plan = {
	administrative_efficiency = -0.1
}

aelnar_adapt_the_army = {
	land_morale = -0.1
}

aelnar_the_auxiliaries = {
	global_manpower_modifier = 0.1
	land_morale = -0.05
}

aelnar_the_starborn = {
	global_manpower_modifier = 0.15
	yearly_army_professionalism = 0.002
	global_unrest = -1
	max_states = 5
	advisor_pool = 1
}

aelnar_adapting_administration = {
	administrative_efficiency = -0.1
}

#Jaddari Modifiers
jaddari_rise_of_an_empire = {
	administrative_efficiency = 0.1
}

jaddari_mounted_legions = {
	cavalry_power = 0.2
	movement_speed = 0.1
	cav_to_inf_ratio = 0.25
	yearly_tribal_allegiance = 1
}

jaddari_harpy_diplomats = {
	diplomatic_reputation = 2
	diplomats = 1
}

jaddari_the_mountain_of_clear_sight = {
	local_missionary_strength = -0.03
	tolerance_own = 1
}

jaddari_the_true_faith = {
	tolerance_own = 2
	prestige = 2
}

jaddari_grand_temple = {
	missionaries = 1
	global_missionary_strength = 0.02
}

jaddari_pilgrimages = {
	tax_income = 12
	local_institution_spread = 0.25
}

jaddari_fortified_pass = {
	local_defensiveness = 0.2
}

jaddari_cash_influx = {
	mercenary_cost = -0.33
	merc_maintenance_modifier = -0.33
	land_maintenance_modifier = -0.2
	build_cost = -0.2
	missionary_maintenance_cost = -0.5
}

jaddari_harpy_march = {
	enemy_core_creation = 0.5
	hostile_attrition = 1
	production_efficiency = 0.1
	development_cost = -0.2
	heir_chance = 0.25
	manpower_recovery_speed = 0.1
	diplomatic_reputation = 2
}

jaddari_lightbringers = {
	global_missionary_strength = 0.02
	tolerance_heathen = -2
}

jaddari_shipbuilding_spree = {
	global_ship_cost = -0.33
	global_ship_recruit_speed = -.33
}

rezankand_sunny_road = {
	local_friendly_movement_speed = 0.1
	local_development_cost = -0.05
	local_institution_spread = 0.1
}

rezankand_build_boat = {
	global_ship_cost = -0.25
	global_ship_recruit_speed = -0.25
}

rezankand_golden_armada = {
	heavy_ship_power = 0.075
}

rezankand_indivisible_people = {
	production_efficiency = 0.1
}

rezankand_consecrated_land = {
	local_missionary_strength = 0.03
	local_culture_conversion_cost = -0.3
	trade_goods_size_modifier = -0.5
	local_unrest = -1
}

rezankand_taychend_riches = {
	monthly_splendor = 0.5
	trade_goods_size = 0.5
}

rezankand_kheionia_riches = {
	monthly_splendor = 0.5
	trade_goods_size = 0.5
}

rezankand_eordand_riches = {
	monthly_splendor = 0.5
	trade_goods_size = 0.5
}

rezankand_colonial_office = {
	global_colonial_growth = 60
	colonist_placement_chance = 0.1
	native_uprising_chance = 0.25
}

rezankand_settlements_edicts = {
	colonists = 1
	colonist_placement_chance = 0.1
}

rezankand_azka_traz = {
	local_defensiveness = 0.33
}

rezankand_ruan_expedition = {
	colonists = 1
	land_morale = 0.1
}

rezankand_kheionia_trade = {
	trade_efficiency = 0.2
}

rezankand_immigration_office = {
	local_development_cost = -0.3
}

rezankand_leaf_path = {
	local_friendly_movement_speed = 0.25
}

rezankand_military_reform = {
	discipline = 0.05
}

rezankand_silver_mine = {
	trade_goods_size = 1.5
}

rezankand_coinage = {
	global_tax_income = 48
	global_tax_modifier = 0.2
	inflation_reduction = -1.5
	inflation_action_cost = 1.5
}

rezankand_logistic_port = {
	trade_goods_size = 2
}

rezankand_inquisition = {
	global_unrest = 1.5
	missionary_maintenance_cost = -0.25
	missionaries = 2
	global_missionary_strength = 0.03
}

rezankand_ynn_grain = {
	global_trade_goods_size_modifier = 0.1
}

rezankand_sunny_day = {
	free_policy = 1
	diplomatic_reputation = 2
	reduced_liberty_desire = 25
}

rezankand_jungle_specialist = {
	local_colonist_placement_chance = 0.05
	local_colonial_growth = 20
}

rezankand_dazinbringer = {
	monarch_military_power = 1
	yearly_absolutism = 0.25
}

rezankand_sun_temple = {
	enforce_religion_cost = -0.3
}

rezankand_recent_immigration = {
	local_development_cost = 0.2
}

rezankand_ai_help = {
	global_colonial_growth = 100
	global_tax_income = 48
}

rezankand_growing_capital = {
	local_development_cost = -0.2
	local_institution_spread = 0.1
}

rezankand_migration = {
	global_regiment_recruit_speed = -0.75
	colonists = -3
}

rezankand_first_expedition = {
	movement_speed = 0.1
	siege_ability = 0.1
	defensiveness = 0.25
	infantry_power = 0.2
	global_supply_limit_modifier = 0.5
	reinforce_speed = 0.5
	warscore_cost_vs_other_religion = -0.5
}

rezankand_pillaged_province = {
	trade_goods_size = -0.5
	local_institution_spread = 0.25
}

rezankand_cortez_legacy = {
	trade_goods_size = 3
	diplomatic_reputation = -1
}

militarizing_the_capital = {
	global_tax_modifier = -0.1
}

rezankand_militarize_census = {
	global_manpower_modifier = 0.05
}

rezankand_militarize_wall = {
	local_defensiveness = 0.1
}

rezankand_officier_academy = {
	free_leader_pool = 1
}

# Dameria

dameria_pride_of_varilor_afloat = {
	prestige_from_naval = 0.25
}

dameria_anbenncost_trade = {
	trade_efficiency = 0.15
}

dameria_dameshead_trade = {
	trade_efficiency = 0.3
}

dameria_damesear_secured = {
	local_defensiveness = 0.2
}

dameria_grand_ballroom = {
	diplomatic_reputation = 1
	diplomatic_upkeep = 2
	imperial_authority = 0.05
	dip_tech_cost_modifier = -0.15
}

damerian_parliament = {
	global_unrest = -1
	improve_relation_modifier = 0.2
}

dameria_ambitions = {
	diplomatic_reputation = 1
	diplomats = 1
}

moon_upon_a_dove_throne = {
	imperial_authority = 0.05
	diplomatic_reputation = 1
	land_morale = 0.05
}

dameria_avenged = {
	land_morale = 0.05
}

dameria_house_of_silmuna = {
	heir_chance = 0.5
}

dameria_diplomatic_corps = {
	envoy_travel_time = -0.33
	diplomats = 1
}

dameria_reforged = {
	reduced_liberty_desire = 10
	prestige = 1
}

damerian_army = {
	infantry_power = 0.2
}

dameria_safeguard_empire = {
	shock_damage_received = -0.3
}

# Wesdam

wesdam_diplomatic_approach = {
	improve_relation_modifier = 0.1
	global_tax_income = -6
}

wesdam_joint_military_exercice = {
	land_morale = 0.1
	movement_speed = 0.1
}

wesdam_strong_marquisat = {
	infantry_power = 0.05
	mil_tech_cost_modifier = -0.1
}

wesdam_lorent_modifier = {
	land_morale = 0.075
}

wesdam_dameria_modifier = {
	diplomatic_upkeep = 1
	diplomats = 1
}

wesdam_only_west_lord = {
	prestige = 1
}

wesdam_damerian_ambition = {	
	diplomatic_reputation = 1
}

wesdam_trade_secured = {
	trade_efficiency = 0.1
}

wesdam_damesear_ruler = {
	all_power_cost = -0.05
}

wesdam_galley_fleet = {
	galley_power = 0.15
}

wesdam_emperor_favour = {
	ae_impact = -0.25
}

# Generic Orc missions

orc_clans_reputation = {
	legitimacy = 1
}
orc_clans_contribution = {
	vassal_forcelimit_bonus = 0.33
	vassal_income = 0.2
}
orc_subjugators = {
	shock_damage = 0.1
}
orc_developing_trade_relations = {
	caravan_power = 0.25
	trade_efficiency = 0.1
}
orc_raised_morale = {
	land_morale = 0.15
}
orc_raiding_parties = {
	tax_income = 6
}
orc_war_industry = {
	production_efficiency = 0.2
	infantry_power = 0.1
}
orc_integration_of_new_lands = {
	core_creation = -0.25
}
orc_growing_numbers = {
	global_manpower = 10
	manpower_recovery_speed = 0.15
}
orc_experience_of_the_greentide = {
	army_tradition = 1
	warscore_cost_vs_other_religion = -0.25
}
orc_time_for_revenge_has_come = {
	shock_damage = 0.1
	land_maintenance_modifier = -0.1
}
orc_years_of_recovery = {
	manpower_recovery = 0.15
}
orc_army_formation = {
	shock_damage_received = -0.1
}
orc_charge_to_victory = {
	shock_damage = 0.1
}
orc_on_the_path_to_civilization = {
	global_institution_spread = 0.2
	embracement_cost = -0.33
}
orc_growing_production = {
	trade_goods_size = 1
}
orc_settling_down = {
	local_development_cost = -0.2
}
orc_rich_harvests = {
	global_tax_modifier = 0.15
}
orc_warg_cavalry = {
	cavalry_power = 0.2
}
orc_cheap_blades = {
	infantry_cost = -0.1
}
orc_first_cannons = {
	artillery_cost = -0.1
}

#Grombar Modifiers

grombar_glorious_victory = {
	core_creation = -0.25
}

grombar_southern_threat = {
	defensiveness = 0.2
}

grombar_assimilation_effort = {
	culture_conversion_cost = -0.25
}

grombar_shrines_to_our_gods = {
	tolerance_own = 2
	monthly_fervor_increase = 1
}

grombar_growing_population = {
	development_cost = -0.20
}

grombar_growing_population = {
	development_cost = -0.20
}

grombar_new_order_forged_from_chaos = {
	stability_cost_modifier = -0.1
}

grombar_sea_wolves = {
	naval_morale = 0.1
	global_sailors_modifier	= 0.2
}

grombar_naval_enthusiasm = {
	naval_morale = 0.1
}

grombar_gror_khodash = {
	prestige = 1
	legitimacy = 1
	republican_tradition = 1
	devotion = 1
}

#Khozrugan Modifiers

khozrugan_in_honor_of_our_god = {
	monthly_fervor_increase = 1
	church_power_modifier = 1
	devotion = 1
}

khozrugan_loyal_clans = {
	vassal_forcelimit_bonus = 0.5
}

khozrugan_defeated_pretender = {
	discipline = 0.1
}

khozrugan_unifiers_of_orcs = {
	land_morale = 0.1
}

khozrugan_prepare_to_conquer_seas = {
	global_ship_recruit_speed = -0.25
}

khozrugan_studying_gnomish_technologies = {
	mil_tech_cost_modifier = -0.1	
}

khozrugan_castanorian_engineering = {
	fort_maintenance_modifier = -0.1
}

khozrugan_united_tribes = {
	global_unrest = -1
}

khozrugan_inspired_warriors = {
	land_morale = 0.1
}

khozrugan_influx_of_warriors = {
	reinforce_speed = 0.2
}

khozrugan_first_victory = {
	manpower_recovery_speed = 0.25
}

khozrugan_crushed_enemy = {
	shock_damage = 0.25
}

khozrugan_impaled_slavers = {
	global_unrest = -1
}

khozrugan_integrating_conquered_lands = {
	global_autonomy = -0.05
}

khozrugan_conquered_trade_centers = {
	trade_efficiency = 0.2
}

khozrugan_victory_is_near = {
	recover_army_morale_speed = 0.1
}

khozrugan_conquerors_of_cannor = {
	global_manpower = 0.25
	legitimacy = 1
	devotion = 1
	republican_tradition = 0.5
	monthly_splendor = 1
	core_creation = -0.1
}

khozrugan_integrating_new_lands = {
	core_creation = 0.25
}

khozrugan_conquered_free_cities = {
	trade_steering = 0.2
}

khozrugan_ibevari_wine = {
	land_morale = 0.1
}

khozrugan_new_fashion_trends = {
	prestige = 0.5
}

khozrugan_most_desired_city_in_the_world = {
	development_cost = -0.2
}

khozrugan_worg_cavalry = {
	cavalry_power = 0.33
}

khozrugan_organized_army = {
	discipline = 0.1
}

khozrugan_new_subjects = {
	core_creation = -0.25
}

# Unguldavor Modifiers

unguldavor_orcish_zeal = {
	monthly_fervor_increase = 1
	church_power_modifier = 1
	devotion = 1
}

unguldavor_orcish_renaissance = {
	global_institution_spread = 0.5
	technology_cost = -0.1
}

unguldavor_khozrugani_warriors = {
	land_morale = 0.15
}

unguldavor_reconstruction_of_castonath = {
	local_development_cost = -0.33
}

unguldavor_civil_district = {
	prestige = 1
}

unguldavor_war_district = {
	local_manpower_modifier = 1.00
	land_forcelimit = 10
}

unguldavor_trade_district = {
	trade_goods_size = 1
}

unguldavor_barumandi_farmlands = {
	global_tax_modifier = 0.15
}

unguldavor_gray_orc_shipbuilding = {
	global_ship_cost = -0.1
}

unguldavor_new_country = {
	adm_tech_cost_modifier = -0.1
}

unguldavor_land_of_castles = {
	fort_maintenance_modifier = -0.1
}

unguldavor_advanced_artillery = {
	siege_ability = 0.2
	artillery_power = 0.1
}

unguldavor_building_new_cities = {
	build_cost = -0.2
}

unguldavor_growing_population = {
	development_cost = -0.1
}

unguldavor_advanced_farming = {
	global_trade_goods_size_modifier = 0.1
}

unguldavor_orcish_industrialization = {
	global_trade_goods_size_modifier = 0.05
	production_efficiency = 0.1
}

unguldavor_steel_hill = {
	trade_goods_size = 2
}

#Barumand Modifiers

barumand_consolidation_of_barumand = {
	core_creation = -0.25
}

barumand_develop_fortifications = {
	fort_maintenance_modifier = -0.2
}

barumand_new_castles = {
	defensiveness = 0.2
}

barumand_benevolent_victor = {
	diplomatic_reputation = 2
}

barumand_human_subjects = {
	advisor_cost = -0.25
}

barumand_trade_flow = {
	trade_efficiency = 0.2
}

barumand_content_population = {
	global_unrest = -1
}

barumand_repopulating_escann = {
	development_cost = -0.1
}

barumand_building_boom = {
	build_cost = -0.2
	build_time = -0.33
}

barumand_prospering_farmers = {
	global_tax_modifier = 0.15
}

barumand_abolitionism = {
	prestige = 1
}

barumand_beacon_of_knowledge = {
	technology_cost = -0.1
}

barumand_growing_capital = {
	local_development_cost = -0.25
}

barumand_dustandar_grand_arena = {
	local_manpower_modifier = 1
}

barumand_dustandar_mint = {
	tax_income = 1
}

verne_moon_eclipsed = {
	legitimacy = 2
	republican_tradition = 2
	devotion = 2
	prestige = 2
	diplomatic_reputation = 2
}

verne_grand_regatta = {
	global_ship_cost = -0.1
	movement_speed_in_fleet_modifier = 0.2
}

verne_now_we_cannot_miss = {
	global_garrison_growth = 0.4
	reinforce_speed = 0.4
	army_tradition_from_battle = 0.05
}

verne_dominate_the_approach = {
	local_ship_repair = 0.2
	trade_value_modifier = 0.5
	province_trade_power_value = 10
}

verne_hunting_eggs = {
	global_tax_income = -2
}

verne_found_eggs = {
	prestige = 1
}

verne_constructing_sanctuary = {
	tax_income = -1
}

verne_sanctuary = {
	local_development_cost = -0.05
	prestige = 2
	legitimacy = 1
}

verne_raising_wyverns = {
	global_tax_income = -2
}

verne_wyvern_riders = {
	shock_damage = 0.2
	shock_damage_received = -0.2
	cavalry_flanking = 0.2
}

verne_gnoll_question = {
	global_unrest = 2
}

verne_new_adventures = {
	range = 0.5
}

verne_stingport_regatta_held = {
	navy_tradition_decay = -0.02
}

arakeprun_precursor_tool_deprivation = {
	local_development_cost = 0.5
}
arakeprun_glorelthiran_guard = {
	army_tradition_decay = -0.01
	navy_tradition_decay = -0.01
}
arakeprun_splendor_of_spring_court = {
	core_creation = -0.15
	global_autonomy = -0.05
}
arakeprun_protector_of_the_spring_court = {
	manpower_recovery_speed = 0.15
}
arakeprun_elarbarc_shipyards = {
	local_ship_cost = -0.1
	ship_recruit_speed  = -0.1
}
arakeprun_seizure_of_einnsag = {
	navy_tradition = 1
}
tuathak_drills = {
	infantry_power = 0.1
}
arakeprun_warmth_of_the_last_days = {
	land_attrition = -0.1
}
arakeprun_thawing_of_glaciers = {
	local_development_cost = -0.075
	trade_goods_size = 0.1
}
randrunnse_trade = {
	trade_efficiency = 0.1
}
arakeprun_expedition_preparation = {
	colonists = -1
	missionaries = -1
}
eor_pledge_to_unite_eordand = {
	num_accepted_cultures = 2
	core_creation = -0.05
}
eor_champions_of_the_fey = {
	years_of_nationalism = -5
	manpower_recovery_speed = 0.075
}
eor_humble = {
	diplomatic_reputation = 1
	stability_cost_modifier = -0.15
	legitimacy = 0.25
	devotion = 0.25
	republican_tradition = 0.25
}
eor_the_apostate = {
	discipline = 0.075
	war_exhaustion = -0.01
	harsh_treatment_cost = -0.2
	diplomatic_reputation = -2
	ae_impact = 0.2
}
eor_expedition_underway = {
	colonists = 1
	colonist_placement_chance = 0.05
	range = 0.15
}
eor_cadhcimn_1 = { #Cadhcimn, the city unearthed
	local_development_cost = -0.25
	local_build_cost = -0.1
	local_production_efficiency = 0.15
	local_tax_modifier = 0.15
	local_institution_spread = 0.15
}
eor_cadhcimn_2 = { #Cadhcimn Hall of Records
	technology_cost = -0.05
}
eor_cadcimn_3 = { #Haunting of Cadhcimn
	local_development_cost = 0.1
	local_defensiveness = 0.1
	local_hostile_attrition = 1
}
eor_cadhcimn_4 = { #Armory
	local_defensiveness = 0.15
	local_manpower_modifier = 0.25
	shock_damage_received = -0.05
}
eor_spring_equinox = {
	legitimacy = 1
	devotion = 1
	republican_tradition = 0.5
	monthly_splendor = 1
	core_creation = -0.1
}
legacy_of_goldtree = {
	manpower_recovery_speed = 0.15
	land_morale = 0.05
	army_tradition = 0.5
	global_unrest = -1
}
elchos_reign_goldtree = {
	legitimacy = 1
	core_creation = -0.1
}
selphereg_precursor_excavation = {
	local_production_efficiency = 0.2
	province_trade_power_modifier = 0.2
}
eor_damestear_summoner = {
	global_trade_goods_size_modifier = 0.1
}
eor_darhan_blossoming = {
	local_manpower_modifier = 0.1
	local_unrest = -2
	local_production_efficiency = 0.1
}
eor_textiles = {
	trade_goods_size = 0.5
	local_production_efficiency = 0.1
}
eor_spring_celebrations = {
	global_unrest = -1
	global_institution_spread = 0.15
}
eor_vernal_arts = {
	technology_cost = -0.05
	idea_cost = -0.05
}
elchos_trade_dominance = {
	trade_efficiency = 0.1
	naval_morale = 0.1
}
peitar_congregation_convened = {
	improve_relation_modifier = 0.2
	land_maintenance_modifier = -0.05
}
peitar_pride_of_the_faithful = {
	recover_army_morale_speed = 0.1
	war_exhaustion = -0.1
}
peitar_inquisition = {
	global_autonomy = -0.05
	legitimacy = 0.25
	devotion = 0.125
	tolerance_own = 2
	global_heretic_missionary_strength = 0.03
	tolerance_heretic = -3
	harsh_treatment_cost = -0.2
	culture_conversion_cost = -0.25
}
peitar_domandrod_communities = {
	global_trade_power = 0.1
	build_cost = -0.2
}
peitar_blessings_of_the_fey = {
	tolerance_own = 2
	prestige = 1
}
peitar_spirit_of_victory = {
	infantry_power = 0.1
	cavalry_power = 0.1
	artillery_power = 0.1
}
pelomar_bitter_rivalry = {
	land_morale = 0.05
	war_exhaustion = -0.01
	global_unrest = -1
}
pelomar_admired_love = {
	prestige_decay = -0.01
	diplomatic_reputation = 1
}
pelomar_honored_alliance = {
	diplomatic_reputation = 1
	improve_relation_modifier = 0.1
	core_creation = -0.1
}
pelomar_bagcatir_salt = {
	defensiveness = 0.1
	trade_goods_size = 1
}
pelomar_arakeprun_border = {
	defensiveness = 0.1
	garrison_size = 0.1
}
pelodard_gannag = {
	local_development_cost = -0.15
	trade_goods_size_modifier = 0.1
}
caamas_shipbuilding_boom = {
	global_ship_cost = -0.1
	naval_maintenance_modifier = -0.1
}
caamas_trade_secured = {
	province_trade_power_modifier = 0.2
}
caamas_merchant_hold = {
	defensiveness = 0.1
	fort_maintenance_modifier = -0.1
	global_trade_power = 0.1
}
caamas_summer_union = {
	cavalry_power = 0.2
	naval_morale = 0.1
	trade_efficiency = 0.1
}
caamas_sarmadfar_trade_dominance = {
	trade_efficiency = 0.1
	naval_maintenance_modifier = -0.2
	naval_forcelimit_modifier = 0.15
}
caamas_floating_fortresses = {
	heavy_ship_power = 0.1
	sailors_recovery_speed = 0.2
}
caamas_haraf_interests = {
	global_colonial_growth = 20
	range = 0.15
}
caamas_integrating_haraf = {
	global_missionary_strength = 0.3
	colonist_placement_chance = 0.05
}
caamas_urdea_samrad = {
	local_development_cost = -0.2
	local_build_cost = -0.2
	province_trade_power_modifier = 0.2
	local_unrest = -5
}


#Orc basic tree modifier
orc_clans_reputation = {
	legitimacy = 1
}
orc_clans_contribution = {
	vassal_forcelimit_bonus = 0.33
	vassal_income = 0.2
}
orc_subjugators = {
	shock_damage = 0.1
}
orc_developing_trade_relations = {
	caravan_power = 0.25
	trade_efficiency = 0.1
}
orc_raised_morale = {
	land_morale = 0.15
}
orc_raiding_parties = {
	tax_income = 6
}
orc_war_industry = {
	production_efficiency = 0.2
	infantry_power = 0.1
}
orc_integration_of_new_lands = {
	core_creation_cost = -0.25
}
orc_growing_numbers = {
	global_manpower = 10
	manpower_recovery = 0.15
}
orc_experience_of_the_greentide = {
	army_tradition = 1
	warscore_cost_vs_other_religion = -0.25
}
orc_time_for_revenge_has_come = {
	shock_damage = 0.1
	land_maintenance_modifier = -0.1
}
orc_years_of_recovery = {
	manpower_recovery = 0.15
}
orc_army_formation = {
	shock_damage_received = -0.1
}
orc_charge_to_victory = {
	shock_damage = 0.1
}
orc_on_the_path_to_civilization = {
	global_institution_spread = 0.2
	embracement_cost = -0.33
}
