# Bulwari Horse Archers
# Pips: 8

type = cavalry
unit_type = tech_bulwari

maneuver = 3
offensive_morale = 2
defensive_morale = 1
offensive_fire = 3
defensive_fire = 0
offensive_shock = 3
defensive_shock = 2

trigger = {
	tag = F46
}