#These are generic Peitar missions

peitar1_missions = {
	slot = 1 
	generic = no
	ai = yes 
	potential = {
		AND = {
			primary_culture = peitar
			NOT = {
				tag = H08
			}
		}
	}
	
	peitar_prepare_the_gladewardens = {
		icon = mission_rb_rein_in_the_highland_clans
		required_missions = { peitar_congregation_of_the_faithful }
		position = 2
		generic = no
		
		trigger = {
			army_size_percentage = 1
			capital_scope = {
				area_for_scope_province = {
					type = all
					owned_by = ROOT 
				}
				base_manpower = 6
			}
		}
		effect = {
			create_general = { tradition = 50 }
		}
	}
	peitar_rein_in_the_wayward_flock = {
		icon = mission_crusade_for_varna
		required_missions = { peitar_prepare_the_gladewardens }
		position = 3
		generic = no
		
		trigger = {
			custom_trigger_tooltip = {
				tooltip = peitar_rein_wayward
				any_known_country = {
					OR = {
						primary_culture = selphereg
						primary_culture = tuathak
						primary_culture = caamas
						primary_culture = snecboth
					}
					OR = {
						is_in_war = {
							attackers = ROOT
							defenders = THIS
						}
						is_in_war = {
							attackers = THIS
							defenders = ROOT
						}
					}
				}
			}
		}
		effect = {
			add_country_modifier = { name = "peitar_pride_of_the_faithful" duration = 5475 }
		}
	}
	peitar_extinguish_heretical_worship = {
		icon = mission_religious
		required_missions = { peitar_rein_in_the_wayward_flock }
		position = 4
		generic = no
		
		trigger = {
			capital_scope = {
				area_for_scope_province = {
					type = all
					owned_by = ROOT 
				}
				has_building = courthouse
			}
			adm_power = 50
			dip_power = 50
		}
		effect = {
			add_country_modifier = { name = "peitar_inquisition" duration = 9125 }
			add_adm_power = -50
			add_dip_power = -50
		}
	}
	
	
}
peitar2_missons = {
	slot = 2 
	generic = no
	ai = yes 
	potential = {
		AND = {
			primary_culture = peitar
			NOT = {
				tag = H08
			}
		}
	}
	
	peitar_congregation_of_the_faithful = {
		icon = mission_alliances
		required_missions = { }
		position = 1
		generic = no
		
		trigger = {
			custom_trigger_tooltip = {
				tooltip = peitar_congregation
				num_of_allies = 2
				calc_true_if = {
					all_ally = {
						primary_culture = peitar
						has_opinion = {
							who = ROOT
							value = 150
						}
					}
					amount = 2
				}
			}
		}
		effect = {
			add_country_modifier = { name = "peitar_congregation_convened" duration = 5475 }
		}
	}
	peitar_four_seasons_under_one = {
		icon = mission_unite_home_region
		required_missions = { 
			peitar_extinguish_heretical_worship
			peitar_service_to_the_fey_lords
		}
		position = 5
		generic = no
		
		trigger = {
			grown_by_states = 6
			num_accepted_cultures = 3
			eordellon = 4
			mil_power = 100
		}
		effect = {
			add_country_modifier = { name = "peitar_spirit_of_victory" duration = 7300 }

		}
	}
}
peitar3_missons = {
	slot = 3 
	generic = no
	ai = yes 
	potential = {
		AND = {
			primary_culture = peitar
			NOT = {
				tag = H08
			}
		}
	}
	
	peitar_seasonal_neutrality = {
		icon = mission_hands_praying
		required_missions = { peitar_congregation_of_the_faithful }
		position = 2
		generic = no
		
		trigger = {
			advisor = theologian
			prestige = 30
		}
		effect = {
			add_devotion = 10
			add_legitimacy = 10
		}
	}
	peitar_domandrod_communities = {
		icon = mission_clear_the_delta
		required_missions = { peitar_seasonal_neutrality }
		position = 3
		generic = no
		
		trigger = {
			development_in_provinces = {
					value = 50
					culture = peitar
			}
		}
		effect = {
			add_country_modifier = { name = "peitar_domandrod_communities" duration = 9125 }
		}
	}
	peitar_service_to_the_fey_lords = {
		icon = mission_rajput_mansabdars
		required_missions = { peitar_domandrod_communities }
		position = 4
		generic = no
		
		trigger = {
			custom_trigger_tooltip = {
				num_of_vassals = 1
				tooltip = peitar_service
					calc_true_if = {
						all_subject_country = {
							NOT = { primary_culture = peitar }
						}
						amount = 1
					}
			}
			treasury = 250
		}
		effect = {
			add_treasury = -250
			add_country_modifier = { name = "peitar_blessings_of_the_fey" duration = 9125 }
		}
	}
}
peitar4_missions = {
	slot = 4 
	generic = no
	ai = yes 
	potential = {
		AND = {
			primary_culture = peitar
			NOT = {
				tag = H08
			}
		}
	}
	
	peitar_sunset = {
		icon = mission_coromandel_expansion
		required_missions = {  }
		position = 1
		generic = no
		
		trigger = {
			owns_core_province = 2037
			NOT = {exists = H02}
		}
		effect = {
			add_adm_power = 100
			add_treasury = 100
			add_prestige = 10
			2037 = {
				change_culture = peitar
				change_province_name = "Granset" #Sunset
				rename_capital = "Granset"
			}
		}
	}
	peitar_wilted_flowers = {
		icon = mission_rb_war_of_the_roses
		required_missions = {  }
		position = 2
		generic = no
		
		trigger = {
			owns_core_province = 2007
			NOT = {exists = H01}
		}
		effect = {
			add_stability = 1
			add_treasury = 100
			add_prestige = 10
			2007 = {
				change_culture = peitar
				change_province_name = "Sakflor" #WiltedFlower
				rename_capital = "Sakflor"
			}
		}
	}
	peitar_fetid_harvest = {
		icon = mission_rb_import_potatoes
		required_missions = {  }
		position = 3
		generic = no
		
		trigger = {
			owns_core_province = 2159
			NOT = {exists = H10}
		}
		effect = {
			add_war_exhaustion = -2
			add_treasury = 100
			add_prestige = 10
			2159 = {
				change_culture = peitar
				change_province_name = "Lebhard" #FetidHarvest
				rename_capital = "Lebhard"
			}
		}
	}
	peitar_final_thawing = {
		icon = mission_rb_settle_the_north
		required_missions = {  }
		position = 4
		generic = no
		
		trigger = {
			owns_core_province = 1775
			NOT = {exists = H16}
		}
		effect = {
			add_dip_power = 100
			add_treasury = 100
			add_prestige = 10
			1775 = {
				change_culture = peitar
				change_province_name = "Darbait" #FinalThawing
				rename_capital = "Darbait"
			}
		}
	}
	peitar_claim_oathsworn_legacy = {
		icon = mission_defeat_mewar
		required_missions = {  }
		position = 5
		generic = no
		
		trigger = {
			owns_core_province = 2049
			NOT = { exists = H08 }
		}
		effect = {
			add_legitimacy = 25
			add_devotion = 10
			add_prestige = 10
			country_event = { id = eordand.3 }
		}
	}
}