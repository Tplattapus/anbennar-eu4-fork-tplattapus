government = monarchy
add_government_reform = feudalism_reform
government_rank = 1
primary_culture = high_lorentish
religion = regent_court
technology_group = tech_cannorian
capital = 114 # Madelaire
national_focus = DIP

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }

1422.1.1 = { set_country_flag = lilac_wars_rose_party }