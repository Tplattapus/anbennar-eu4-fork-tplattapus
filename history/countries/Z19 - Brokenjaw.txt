government = tribal
add_government_reform = greentide_horde
government_rank = 1
primary_culture = gray_orc
religion = great_dookan
technology_group = tech_orcish
national_focus = DIP
historical_rival = Z18
capital = 989

1427.1.7 = {
	monarch = {
		name = "Jayuk"
		dynasty = "Broken Jaw"
		birth_date = 1427.2.19
		adm = 5
		dip = 2
		mil = 1
	}
}