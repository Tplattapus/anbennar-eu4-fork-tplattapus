# No previous file for Mtetwa

owner = B23
controller = B23
add_core = B23
culture = green_orc
religion = great_dookan
capital = ""

hre = no

base_tax = 2
base_production = 2
base_manpower = 1

trade_goods = livestock

native_size = 30
native_ferocity = 7
native_hostileness = 5