#351 - Biskra

owner = A32
controller = A32
add_core = A32
culture = moon_elf
religion = elven_forebears

hre = no

base_tax = 9
base_production = 9
base_manpower = 5

trade_goods = cloth

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish