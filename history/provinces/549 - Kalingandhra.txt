#549 - Birsartansbar

owner = F21
controller = F21
add_core = F21
culture = bahari
religion = bulwari_sun_cult

base_tax = 7
base_production = 6
base_manpower = 4

trade_goods = grain

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari


add_permanent_province_modifier = {
	name = elven_minority_integrated_large
	duration = -1
}