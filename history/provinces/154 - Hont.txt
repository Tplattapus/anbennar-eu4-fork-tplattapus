#154 - Hont | Exwes-by-the-Sea

owner = A40
controller = A40
add_core = A40
culture = exwesser
religion = regent_court

hre = yes

base_tax = 5
base_production = 5
base_manpower = 3

trade_goods = fish
capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold

