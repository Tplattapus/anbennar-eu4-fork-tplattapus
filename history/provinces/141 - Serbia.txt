#141 - Serbia

owner = A05
controller = A05
add_core = A05
culture = redfoot_halfling
religion = regent_court
hre = no
base_tax = 5
base_production = 3
trade_goods = cloth
center_of_trade = 1
base_manpower = 3
capital = "" 
is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold

add_permanent_province_modifier = {
	name = human_minority_coexisting_small
	duration = -1
}