# No previous file for Seinath�l
owner = A90
controller = A90
add_core = A90
culture = esmari
religion = regent_court

hre = yes

base_tax = 6
base_production = 6
base_manpower = 5

trade_goods = paper

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

add_permanent_province_modifier = {
	name = half_elven_minority_coexisting_small
	duration = -1
}