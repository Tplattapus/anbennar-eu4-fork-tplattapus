# No previous file for Envermarck
owner = Z24
controller = Z24
add_core = Z24
culture = blue_reachman
religion = regent_court

hre = no

base_tax = 4
base_production = 4
base_manpower = 2

trade_goods = fish

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_kobold