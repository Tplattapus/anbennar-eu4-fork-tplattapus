#10 - Osterbotten | Eil�sin

owner = A15 #Duchy of Eil�sin
controller = A15
add_core = A15
culture = west_damerian
religion = regent_court

hre = yes

base_tax = 4
base_production = 4
base_manpower = 3

trade_goods = cloth

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
