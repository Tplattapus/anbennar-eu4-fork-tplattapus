# No previous file for Baywic
owner = Z28
controller = Z28
add_core = Z28
culture = blue_reachman
religion = regent_court

hre = no

base_tax = 5
base_production = 4
base_manpower = 3

trade_goods = glass
center_of_trade = 1

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_kobold