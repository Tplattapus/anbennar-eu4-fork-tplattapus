# No previous file for Baoding
owner = Z22
controller = Z22
add_core = Z22
culture = moon_elf
religion = regent_court

hre = no

base_tax = 6
base_production = 6
base_manpower = 4

trade_goods = cloth
center_of_trade = 2

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold
discovered_by = tech_orcish

add_permanent_province_modifier = {
	name = human_minority_coexisting_large
	duration = -1
}