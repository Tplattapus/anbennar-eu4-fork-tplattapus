# 310 - Novgorod

owner = A60
controller = A60
add_core = A60
culture = arannese
religion = regent_court

hre = yes

base_tax = 4
base_production = 4
base_manpower = 4

trade_goods = grain

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish


add_permanent_province_modifier = {
	name = half_elven_minority_integrated_small
	duration = -1
}