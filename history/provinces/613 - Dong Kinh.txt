# No previous file for Dong Kinh
owner = F35
controller = F35
add_core = F35
culture = zanite
religion = bulwari_sun_cult

hre = no

base_tax = 9
base_production = 9
base_manpower = 4

trade_goods = grain
center_of_trade = 1

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin


add_permanent_province_modifier = {
	name = elven_minority_integrated_large
	duration = -1
}
discovered_by = tech_bulwari