namespace = government_events

# Constitutional Restoration
country_event = {
	id = 9479
	title = "EVTNAME9479"
	desc = "EVTDESC9479"
	picture = COURT_eventPicture
	
	trigger = {
		has_country_modifier = separation_of_powers
		government = monarchy
		has_reform_level = 5
		NOT = {
			has_reform = parliamentary_reform
		}
		NOT = {
			has_reform = constitutional_monarchy
		}
		NOT = { has_reform = celestial_empire }
		NOT = { has_reform = dwarven_monarchy }
	}
	
	mean_time_to_happen = {
		months = 12
	}
	
	option = {
		name = "EVTOPTA9479"
		remove_country_modifier = separation_of_powers
		add_stability = -2
		random_owned_province = {
			limit = {
				is_core = ROOT
			}
			noble_rebels = 3
		}
	}
	
	option = {
		name = "EVTOPTB9479"
		add_government_reform = parliamentary_reform
		add_stability_or_adm_power = yes
	}
}

# The worst excesses stop
country_event = {
	id = 9480
	title = "EVTNAME9480"
	desc = "EVTDESC9480"
	picture = CITY_DEVELOPMENT_eventPicture
	
	trigger = {
		has_country_modifier = cult_of_reason
		NOT = {
			has_reform = revolutionary_republic_reform
		}
	}
	
	mean_time_to_happen = {
		months = 12
	}
	
	option = {
		name = "EVTOPTA9480"
		add_stability_or_adm_power = yes
		remove_country_modifier = cult_of_reason
	}
}

# Free City status lost (for non Common Sense players)
country_event = {
	id = 9900
	title = free_cities.2.t
	desc = free_cities.2.d
	picture = HRE_eventPicture
	
	trigger = {
		NOT = { has_dlc = "Common Sense" }
		has_reform = free_city
		num_of_cities = 2
	}
	
	mean_time_to_happen = {
		months = 12
	}

	option = {
		name = free_cities.2.a
		remove_government_reform = free_city
	}
}

# Iqta & Mamluk Government no longer Muslim
country_event = {
	id = government_events.1
	title = government_events.1.t
	desc = government_events.1.d
	picture = COURT_eventPicture
	
	trigger = {
		OR = {
			has_reform = iqta
			has_reform = mamluk_government
			has_reform = indian_sultanate_reform
		}
		NOT = { religion_group = muslim }
	}
	
	mean_time_to_happen = {
		months = 12
	}

	option = {
		name = government_events.1.a
		remove_government_reform = iqta
		remove_government_reform = mamluk_government
		remove_government_reform = indian_sultanate_reform
	}
}