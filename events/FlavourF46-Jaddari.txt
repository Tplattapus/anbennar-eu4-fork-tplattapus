
namespace = jaddari_missions

#Reform the Cult
country_event = {
	id = jaddari_missions.1
	title = jaddari_missions.1.t
	desc = jaddari_missions.1.d
	picture = POPE_PREACHING_eventPicture
	
	is_triggered_only = yes
	
	#Convert
	option = {
		name = jaddari_missions.1.a
		ai_chance = {
			factor = 40 
			modifier = {
				factor = 2
				NOT = {
					primary_culture = sun_elf
					primary_culture = zanite
					primary_culture = brasanni
					primary_culture = bahari
					primary_culture = gelkar
				}
			}
		}

		change_religion = the_jadd
		
		add_country_modifier = {
			name = "conversion_zeal"
			duration = 3650
		}
	}
	
	#Don't convert: Tolerant
	option = {
		name = jaddari_missions.1.b
		ai_chance = {
			factor = 30 
			modifier = {
				factor = 2
				primary_culture = sun_elf
			}
		}
	
		add_country_modifier = {
			name = tolerant_society
			duration = 3650
		}
	}
	
	#Don't convert: Harsh
	option = {
		name = jaddari_missions.1.b
		ai_chance = {
			factor = 30 
			modifier = {
				factor = 2
				primary_culture = sun_elf
			}
		}
		
		add_country_modifier = {
			name = intolerant_society
			duration = 3650
		}
	}
}

#A harpy march
country_event = {
	id = jaddari_missions.2
	title = jaddari_missions.2.t
	desc = jaddari_missions.2.d
	picture = WESTERNISATION_eventPicture
	
	is_triggered_only = yes
	
	#Accept
	option = {
		name = jaddari_missions.2.a
		ai_chance = { factor = 100 }
		
		2903 = {
			cede_province = F49
			add_core = F49
			remove_core = F46
		}
		2904 = {
			cede_province = F49
			add_core = F49
			remove_core = F46
		}
		2906 = {
			cede_province = F49
			add_core = F49
			remove_core = F46
		}
		2908 = {
			cede_province = F49
			add_core = F49
			remove_core = F46
		}
		2910 = {
			cede_province = F49
			add_core = F49
			remove_core = F46
		}
		2919 = {
			cede_province = F49
			add_core = F49
			remove_core = F46
		}
		2920 = {
			cede_province = F49
			add_core = F49
			remove_core = F46
		}
		2921 = {
			cede_province = F49
			add_core = F49
			remove_core = F46
		}
		F46 = { create_march = F49 }
		F49 = {
			add_country_modifier = {
				name = jaddari_harpy_march
				duration = -1
			}
		}
		hidden_effect = {
			every_country = {
				remove_opinion = {
					who = ROOT
					modifier = root_monstrous
				}
				reverse_remove_opinion = {
					who = ROOT
					modifier = root_monstrous
				}
			}
			remove_opinion = {
				who = F46
				modifier = jaddari_jasiene
			}
			reverse_remove_opinion = {
				who = F46
				modifier = jaddari_jasiene
			}
			F46 = {
				country_event = {
					id = jaddari_missions.3
					days = 1
				}
			}
		}
		
	}
	
	#Reject
	option = {
		name = jaddari_missions.2.b
		ai_chance = { factor = 0 }
	
		add_trust = {
			who = F46
			value = -25
			mutual = yes
		}
	}
}

#Elayenna accepted
country_event = {
	id = jaddari_missions.3
	title = jaddari_missions.3.t
	desc = jaddari_missions.3.d
	picture = WESTERNISATION_eventPicture
	
	is_triggered_only = yes
	
	#Convert
	option = {
		name = jaddari_missions.1.a
	}
}