
namespace = cyranvar

#Death of Cyranvar King
country_event = {
	id = cyranvar.1
	title = cyranvar.1.t
	desc = cyranvar.1.d
	picture = ROYAL_COUPLE_FUTURE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		has_reform = oak_monarchy_reform
	}
	
	immediate = {
		hidden_effect = {
			random_list = {
				50 = { set_country_flag = house_1 }
				50 = { set_country_flag = house_2 }
			}
			random_list = {
				50 = { set_country_flag = house_3 }
				50 = { set_country_flag = house_4 }
			}
			random_list = {
				50 = { set_country_flag = house_5 }
				50 = { set_country_flag = house_6 }
			}
			random_list = {
				33 = { set_country_flag = house_7 }
				33 = { set_country_flag = house_8 }
				33 = { set_country_flag = house_9 }
			}
		}
	}
	
	#House X
	option = {		
		name = cyranvar.1.a
		ai_chance = { factor = 25 }	
		trigger = { has_country_flag = house_1 }
		define_ruler = {
			age = 67
			claim = 90
			adm = 0
			dip = 0
			mil = 0
			max_random_adm = 6
			max_random_dip = 6
			max_random_mil = 6
		}
		hidden_effect = {
			clr_country_flag = house_1
			clr_country_flag = house_2
			clr_country_flag = house_3
			clr_country_flag = house_4
			clr_country_flag = house_5
			clr_country_flag = house_6
			clr_country_flag = house_7
			clr_country_flag = house_8
			clr_country_flag = house_9
		}
	}
	
	#House X
	option = {		
		name = cyranvar.1.b
		ai_chance = { factor = 25 }	
		trigger = { has_country_flag = house_2 }
		define_ruler = {
			age = 64
			claim = 90
			adm = 0
			dip = 0
			mil = 0
			max_random_adm = 6
			max_random_dip = 6
			max_random_mil = 6
		}
		hidden_effect = {
			clr_country_flag = house_1
			clr_country_flag = house_2
			clr_country_flag = house_3
			clr_country_flag = house_4
			clr_country_flag = house_5
			clr_country_flag = house_6
			clr_country_flag = house_7
			clr_country_flag = house_8
			clr_country_flag = house_9
		}
	}
	
	#House X
	option = {		
		name = cyranvar.1.c
		ai_chance = { factor = 25 }	
		trigger = { has_country_flag = house_3 }
		define_ruler = {
			age = 61
			claim = 90
			adm = 0
			dip = 0
			mil = 0
			max_random_adm = 6
			max_random_dip = 6
			max_random_mil = 6
		}
		hidden_effect = {
			clr_country_flag = house_1
			clr_country_flag = house_2
			clr_country_flag = house_3
			clr_country_flag = house_4
			clr_country_flag = house_5
			clr_country_flag = house_6
			clr_country_flag = house_7
			clr_country_flag = house_8
			clr_country_flag = house_9
		}
	}
	
	#House X
	option = {		
		name = cyranvar.1.e
		ai_chance = { factor = 25 }	
		trigger = { has_country_flag = house_4 }
		define_ruler = {
			age = 65
			claim = 90
			adm = 0
			dip = 0
			mil = 0
			max_random_adm = 6
			max_random_dip = 6
			max_random_mil = 6
		}
		hidden_effect = {
			clr_country_flag = house_1
			clr_country_flag = house_2
			clr_country_flag = house_3
			clr_country_flag = house_4
			clr_country_flag = house_5
			clr_country_flag = house_6
			clr_country_flag = house_7
			clr_country_flag = house_8
			clr_country_flag = house_9
		}
	}
	
	#House X
	option = {		
		name = cyranvar.1.f
		ai_chance = { factor = 25 }	
		trigger = { has_country_flag = house_5 }
		define_ruler = {
			age = 71
			claim = 90
			adm = 0
			dip = 0
			mil = 0
			max_random_adm = 6
			max_random_dip = 6
			max_random_mil = 6
		}
		hidden_effect = {
			clr_country_flag = house_1
			clr_country_flag = house_2
			clr_country_flag = house_3
			clr_country_flag = house_4
			clr_country_flag = house_5
			clr_country_flag = house_6
			clr_country_flag = house_7
			clr_country_flag = house_8
			clr_country_flag = house_9
		}
	}
	
	#House X
	option = {		
		name = cyranvar.1.g
		ai_chance = { factor = 25 }	
		trigger = { has_country_flag = house_6 }
		define_ruler = {
			age = 59
			claim = 90
			adm = 0
			dip = 0
			mil = 0
			max_random_adm = 6
			max_random_dip = 6
			max_random_mil = 6
		}
		hidden_effect = {
			clr_country_flag = house_1
			clr_country_flag = house_2
			clr_country_flag = house_3
			clr_country_flag = house_4
			clr_country_flag = house_5
			clr_country_flag = house_6
			clr_country_flag = house_7
			clr_country_flag = house_8
			clr_country_flag = house_9
		}
	}
	
	#House X
	option = {		
		name = cyranvar.1.h
		ai_chance = { factor = 25 }	
		trigger = { has_country_flag = house_7 }
		define_ruler = {
			age = 69
			claim = 90
			adm = 0
			dip = 0
			mil = 0
			max_random_adm = 6
			max_random_dip = 6
			max_random_mil = 6
		}
		hidden_effect = {
			clr_country_flag = house_1
			clr_country_flag = house_2
			clr_country_flag = house_3
			clr_country_flag = house_4
			clr_country_flag = house_5
			clr_country_flag = house_6
			clr_country_flag = house_7
			clr_country_flag = house_8
			clr_country_flag = house_9
		}
	}
	
	#House X
	option = {		
		name = cyranvar.1.i
		ai_chance = { factor = 25 }	
		trigger = { has_country_flag = house_8 }
		define_ruler = {
			age = 64
			claim = 90
			adm = 0
			dip = 0
			mil = 0
			max_random_adm = 6
			max_random_dip = 6
			max_random_mil = 6
		}
		hidden_effect = {
			clr_country_flag = house_1
			clr_country_flag = house_2
			clr_country_flag = house_3
			clr_country_flag = house_4
			clr_country_flag = house_5
			clr_country_flag = house_6
			clr_country_flag = house_7
			clr_country_flag = house_8
			clr_country_flag = house_9
		}
	}
	
	#House X
	option = {		
		name = cyranvar.1.j
		ai_chance = { factor = 25 }	
		trigger = { has_country_flag = house_9 }
		define_ruler = {
			age = 62
			claim = 90
			adm = 2
			dip = 2
			mil = 2
			max_random_adm = 6
			max_random_dip = 6
			max_random_mil = 6
			female = yes
		}
		hidden_effect = {
			clr_country_flag = house_1
			clr_country_flag = house_2
			clr_country_flag = house_3
			clr_country_flag = house_4
			clr_country_flag = house_5
			clr_country_flag = house_6
			clr_country_flag = house_7
			clr_country_flag = house_8
			clr_country_flag = house_9
		}
	}
}

#Select parliament or absolute ruler
country_event = {
	id = cyranvar.2
	title = cyranvar.2.t
	desc = cyranvar.2.d
	picture = ROYAL_COUPLE_FUTURE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}
	#Parliament
	option = {		
		name = cyranvar.2.a
		ai_chance = { factor = 40 }
		hidden_effect = { set_country_flag = cyranvar_parliament_reform }
		add_government_reform = cyranvar_parliament_reform
		custom_tooltip = cyranvar_parliament_tooltip
	}
	#Absolute ruler
	option = {		
		name = cyranvar.2.a
		ai_chance = { factor = 60 }
		add_country_modifier = {
			name = cyranvar_absolute_ruler
			duration = -1
		}
		custom_tooltip = cyranvar_absolute_ruler_tooltip
	}
}

#Isolationist or Open Border path
country_event = {
	id = cyranvar.3
	title = cyranvar.3.t
	desc = cyranvar.3.d
	picture = ROYAL_COUPLE_FUTURE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}
	#Isolationist
	option = {		
		name = cyranvar.3.a
		ai_chance = { factor = 40 }
		hidden_effect = { set_country_flag = cyranvar_isolationist_path }
		custom_tooltip = cyranvar_isolationist_tooltip
	}
	#Open Border
	option = {		
		name = cyranvar.3.b
		ai_chance = { factor = 60 }
		hidden_effect = { set_country_flag = cyranvar_open_path }
		custom_tooltip = cyranvar_open_border_tooltip
	}
}

#Submit to Cyranvar ?
country_event = {
	id = cyranvar.4
	title = cyranvar.4.t
	desc = cyranvar.4.d
	picture = ROYAL_COUPLE_FUTURE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}
	#Submit
	option = {		
		name = cyranvar.4.a
		ai_chance = { factor = 50 }
		FROM = { country_event = { id = cyranvar.5 days = 20 random = 10 } }
	}
	#Never
	option = {		
		name = cyranvar.4.b
		ai_chance = { factor = 50 }
		FROM = { country_event = { id = cyranvar.6 days = 20 random = 10 } }
	}
}

#They submitted
country_event = {
	id = cyranvar.5
	title = cyranvar.5.t
	desc = cyranvar.5.d
	picture = ROYAL_COUPLE_FUTURE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}

	option = {		
		name = cyranvar.5.a
		ai_chance = { factor = 40 }
		vassalize = FROM
	}
}

#They refuse to submit
country_event = {
	id = cyranvar.6
	title = cyranvar.6.t
	desc = cyranvar.6.d
	picture = ROYAL_COUPLE_FUTURE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}

	option = {
		name = cyranvar.6.a
		ai_chance = { factor = 40 }
		add_casus_belli = {
			target = FROM
			type = cb_vassalize_mission
			months = 360
		}
	}
}

#######################
#Debug and background events
#######################
country_event = {
	id = cyranvar.100
	title = cyranvar.100.t
	desc = cyranvar.100.d
	picture = ROYAL_COUPLE_FUTURE_eventPicture
	
	hidden = yes
	
	mean_time_to_happen = {
		years = 1
	}
	
	trigger = {
		has_reform = oak_monarchy_reform
		if = {
			limit = { has_country_modifier = cyranvar_absolute_ruler }
			ruler_age = 150
		}
		else = {
			ruler_age = 100
		}
	}
	
	option = {		
		name = cyranvar.100.a
		ai_chance = { factor = 25 }	
		country_event = { id = cyranvar.1 }
	}
}

